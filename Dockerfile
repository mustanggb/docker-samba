FROM alpine:3.17
ARG ARCHITECTURE=x86_64
ARG S6_OVERLAY_VERSION=3.1.5.0

# Install new packages
RUN apk add --no-cache docker samba-server samba-common-tools jq

# s6 install
# Based on harningt/docker-base-alpine-s6-overlay
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-noarch.tar.xz /tmp/
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-${ARCHITECTURE}.tar.xz /tmp/
RUN tar -xJpC / -f /tmp/s6-overlay-noarch.tar.xz && \
    tar -xJpC / -f /tmp/s6-overlay-${ARCHITECTURE}.tar.xz && \
    rm /tmp/s6-overlay-noarch.tar.xz && \
    rm /tmp/s6-overlay-${ARCHITECTURE}.tar.xz

# Docker share install
ADD docker-share /usr/local/bin/docker-share
RUN chmod +x /usr/local/bin/docker-share

# s6 config
COPY init/ /etc/s6-overlay/s6-init.d/
COPY services/ /etc/s6-overlay/s6-rc.d/
ENV PATH="${PATH}:/command"

# Samba config
ADD smb.conf /etc/samba/smb.conf

# Expose ports
EXPOSE 139 445

# Run s6
ENTRYPOINT /init

**docker-compose.yml**
```
services:
  share:
    image: registry.gitlab.com/mustanggb/docker-samba:latest
    ports:
      - 139:139
      - 445:445
    environment:
      CREDENTIALS: username:password
      FORCE: user(uid):group(gid)
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - /var/lib/docker:/var/lib/docker
  my-service:
    image: my-image
    labels:
      samba: /my-share-path

```

Multiple credentials can be used, e.g. `CREDENTIALS: 'username1:password1 username2:password2'`.

Automatic uid and gid are available e.g. `FORCE: user:group`.

Multiple share paths per container are currently _not_ possible.
